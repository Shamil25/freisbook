import React from 'react';
import Header from '../header/Header.jsx';
import Navbar from '../navbar/Navbar.jsx';
import CurrentPage from '../currentPage/CurrentPage.jsx';

const MainPage = () => {
    return (
        <>
            <Header />
            <Navbar />
            <CurrentPage />
        </>
    )
}

export default React.memo(MainPage);
